<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Our Offer</title>
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,600,700" rel="stylesheet" type="text/css">
		<link href="<?PHP echo base_url(); ?>fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?PHP echo base_url(); ?>css/animate.min.css">
		<link rel="stylesheet" href="<?PHP echo base_url(); ?>style.css">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>

	<body>
		
		<div id="site-content">
			
			<header class="site-header wow fadeInDown">
				<div class="container">
					<div class="header-content">
						<div class="branding">
							<img src="<?PHP echo base_url(); ?>images/logo.png" alt="Company Name" class="logo">
							<h1 class="site-title"><a href="index.html">Gran Viajes</a></h1>
							<small class="site-description">Travel Agency</small>
						</div>
						
						<nav class="main-navigation">
							<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
							<ul class="menu">
								<li class="menu-item"><a href="?page=about-us">About us</a></li>
								<li class="menu-item"><a href="?page=our-offers">Tours</a></li>
								<li class="menu-item"><a href="#">Services</a></li>
								<li class="menu-item"><a href="?page=passport_processing">Passport Processing</a></li>
								<li class="menu-item"><a href="?page=travel_documents">Travel Documents</a></li>
								<li class="menu-item"><a href="?page=visa_assistance">Visa Assistance</a></li>
								<li class="menu-item"><a href="?page=contact">Contact Us</a></li>
							</ul>
						</nav>
						
						<!-- <div class="social-links">
							<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
							<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
							<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
							<a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
						</div> -->
					</div>
					<nav class="breadcrumbs">
						<a href="index.html">Home</a> &rarr;
						<span>Our Offer</span>
					</nav>
				</div>
			</header> <!-- .site-header -->

			<main class="content">
				<div class="fullwidth-block">
					<div class="container">
						<div class="filter-links filterable-nav">
							<select class="mobile-filter">
								<option value="*">Show all</option>
								<option value=".south-america">South America</option>
								<option value=".asia">Asia</option>
								<option value=".africa">Africa</option>
								<option value=".north-america">North America</option>
								<option value=".europe">Europe</option>
								<option value=".australia">Australia</option>	
							</select>
							<a href="#" class=" current wow fadeInRight" data-filter="*">Show all</a>
							<a href="#" class="wow fadeInRight" data-wow-delay=".2s" data-filter=".intl">International</a>
							<a href="#" class="wow fadeInRight" data-wow-delay=".4s" data-filter=".local">Local</a>
<!-- 							<a href="#" class="wow fadeInRight" data-wow-delay=".6s" data-filter=".africa">Africa</a>
							<a href="#" class="wow fadeInRight" data-wow-delay=".8s" data-filter=".north-america">North America</a>
							<a href="#" class="wow fadeInRight" data-wow-delay="1s" data-filter=".europe">Europe</a>
							<a href="#" class="wow fadeInRight" data-wow-delay="1.2s" data-filter=".australia">Australia</a> -->
						</div>
						<div class="filterable-items">
							<div class="filterable-item intl">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-1.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Hongkong</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item intl">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-2.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Japan</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item intl">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-3.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Egypt</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item local">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-1.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Bohol</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item local">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-2.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Cebu</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<!-- <div class="filterable-item australia">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-3.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Palawan</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item south-america">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-1.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Efficitur efficitur convallis</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item asia">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-2.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Efficitur efficitur convallis</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item africa">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-3.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Efficitur efficitur convallis</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item north-america">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-1.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Efficitur efficitur convallis</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item europe">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-2.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Efficitur efficitur convallis</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div>
							<div class="filterable-item australia">
								<article class="offer-item">
									<figure class="featured-image">
										<img src="<?PHP echo base_url(); ?>dummy/offer-thumbnail-3.jpg" alt="">
									</figure>
									<h2 class="entry-title"><a href="#">Efficitur efficitur convallis</a></h2>
									<p>Sed vitae fermentum lacus in augue massa pellentesque mauris vel iaculis sclerisque nulla</p>
									<div class="price">
										<strong>$2900</strong>
										<small>/10 days</small>
									</div>
								</article>
							</div> -->
						</div>

						<!-- <div class="pagination wow fadeInUp">
							<span class="page-numbers current">1</span>
							<a href="#" class="page-numbers">2</a>
							<a href="#" class="page-numbers">3</a>
							<a href="#" class="page-numbers">4</a>
						</div> -->

					</div>

				</div>

				
			</main> <!-- .content -->

			<footer class="site-footer wow fadeInUp">
				<div class="footer-top">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-6">
								<div class="widget">
									<h3 class="widget-title">About us</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus animi asperiores magnam ducimus laboriosam soluta, odio doloribus, voluptas numquam facilis consectetur nam in repudiandae commodi odit iste sed doloremque repellat.</p>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="widget">
									<h3 class="widget-title">Helpful Links</h3>
									<ul class="list-arrow">
										<li><a href="#">Labore et dolore magnam</a></li>
										<li><a href="#">Dolore magnam</a></li>
										<li><a href="#">Magnam Labore et</a></li>
										<li><a href="#">Dolore mabore magnam</a></li>
										<li><a href="#">Et dolore magnam</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="widget">
									<h3 class="widget-title">Helpful Links</h3>
									<ul class="list-arrow">
										<li><a href="#">Labore et dolore magnam</a></li>
										<li><a href="#">Dolore magnam</a></li>
										<li><a href="#">Magnam Labore et</a></li>
										<li><a href="#">Dolore mabore magnam</a></li>
										<li><a href="#">Et dolore magnam</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="widget widget-customer-info">
									<h3 class="widget-title">Customer Service</h3>
									<img src="<?PHP echo base_url(); ?>dummy/footer-customer-service.jpg" alt="" class="pull-left">
									<div class="cs-info">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim voluptates pariatur vero.</p>
										<p>+1 421 458 321 <br> <a href="mailto:cs@companyname.com">cs@companyname.com</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-bottom">
					<div class="container">
						<div class="branding pull-left">
							<img src="<?PHP echo base_url(); ?>images/logo-footer.png" alt="Company Name" class="logo">
							<h1 class="site-title"><a href="index.html">Gran Viajes</a></h1>
							<small class="site-description">Travel Agency</small>
						</div>

						<div class="contact-links pull-right">
							<a href="https://goo.gl/maps/oQKxg"><i class="fa fa-map-marker"></i> 983 Avenue Street, New York</a>
							<a href="tel:+134453455345"><i class="fa fa-phone"></i> +1 344 5345 5345</a>
							<a href="mailto:contact@companyname.com"><i class="fa fa-envelope"></i> contact@companyname.com</a>
						</div>
					</div>
				</div>
				<div class="colophon">
					<div class="container">
						<p class="copy">Copyright 2014 Company Name, Designed by Themezy, All right reserved.</p>
					</div>
				</div>
			</footer> <!-- .site-footer -->

		</div> <!-- #site-content -->
		<script src="<?PHP echo base_url(); ?>js/jquery-1.11.1.min.js"></script>
		<script src="<?PHP echo base_url(); ?>js/min/plugins-min.js"></script>
		<script src="<?PHP echo base_url(); ?>js/min/app-min.js"></script>
		
	</body>

</html>
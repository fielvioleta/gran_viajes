<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Customer Protection</title>
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,600,700" rel="stylesheet" type="text/css">
		<link href="<?PHP echo base_url(); ?>fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?PHP echo base_url(); ?>css/animate.min.css">
		<link rel="stylesheet" href="<?PHP echo base_url(); ?>style.css">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>

	<body>
		
		<div id="site-content">
			
			<header class="site-header wow fadeInDown">
				<div class="container">
					<div class="header-content">
						<div class="branding">
							<img src="<?PHP echo base_url(); ?>images/logo.png" alt="Company Name" class="logo">
							<h1 class="site-title"><a href="index.html">Gran Viajes</a></h1>
							<small class="site-description">Travel Agency</small>
						</div>
						
						<nav class="main-navigation">
							<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
							<ul class="menu">
								<li class="menu-item"><a href="?page=about-us">About us</a></li>
								<li class="menu-item"><a href="?page=our-offers">Tours</a></li>
								<li class="menu-item"><a href="#">Services</a></li>
								<li class="menu-item"><a href="?page=passport_processing">Passport Processing</a></li>
								<li class="menu-item"><a href="?page=travel_documents">Travel Documents</a></li>
								<li class="menu-item"><a href="?page=visa_assistance">Visa Assistance</a></li>
								<li class="menu-item"><a href="?page=contact">Contact Us</a></li>
							</ul>
						</nav>
					</div>
					<nav class="breadcrumbs">
						<a href="index.html">Home</a> &rarr;
						<span>Passport processing</span>
					</nav>
				</div>
			</header> <!-- .site-header -->

			<main class="content">
				<div class="fullwidth-block">
					<div class="container">
						<div class="row">
							<div class="col-md-8 wow fadeInLeft">
								<h2 class="section-title">Passport Processing</h2>
								<p>The most indispensable document you need to have when you travel to other countries is the Passport.  Without the passport, you will not be allowed entry in any foreign country.</p> 

								<p>For Filipino citizens, the government agency in charge of processing and issuing passports is the Department of Foreign Affairs (DFA). Passport applications may be forwarded to the DFA main office in Pasay City or to any of its regional consular offices in San Fernando, La Union; Tuguegarao, Cagayan Valley; Angeles, Pampanga; Lucena City; Legaspi City; Iloilo City; Cebu City; Tacloban City; Zamboanga City; Cagayan de Oro City; and Davao City.</p>

								<p>We are accredited by the DFA to transact business with them in behalf of clients who require passport application and processing assistance. For a minimal service fee we shall properly guide and assist you on the procedures and documentation required by the DFA for passport applicants.</p>

								<div class="row" >
									<div class="col-md-3 col-xs-6">
									</div>
								</div>
							</div>
							<div class="col-md-3 col-md-push-1 wow fadeInRight">
								<h2 class="section-title">New Offer</h2>
								<ul class="list-arrow alt">
									<li><a href="#">Japan</a></li>
									<li><a href="#">Hongkong</a></li>
									<li><a href="#">Egypt</a></li>
									<li><a href="#">Bohol</a></li>
									<li><a href="#">Cebu</a></li>
								</ul>
							</div>
						</div>

					</div>

				</div>

				
			</main> <!-- .content -->

			<footer class="site-footer wow fadeInUp">
				<div class="footer-bottom">
					<div class="container">
						<div class="branding pull-left">
							<img src="<?PHP echo base_url(); ?>images/logo-footer.png" alt="Company Name" class="logo">
							<h1 class="site-title"><a href="index.html">Gran Viajes</a></h1>
							<small class="site-description">Travel Agency</small>
						</div>

						<div class="contact-links pull-right">
							<a href="https://goo.gl/maps/oQKxg"><i class="fa fa-map-marker"></i> 983 Avenue Street, New York</a>
							<a href="tel:+134453455345"><i class="fa fa-phone"></i> +1 344 5345 5345</a>
							<a href="mailto:contact@companyname.com"><i class="fa fa-envelope"></i> contact@companyname.com</a>
						</div>
					</div>
				</div>
				<div class="colophon">
					<div class="container">
						<p class="copy">Copyright 2014 Company Name, Designed by Themezy, All right reserved.</p>
					</div>
				</div>
			</footer> <!-- .site-footer -->

		</div> <!-- #site-content -->
		<script src="<?PHP echo base_url(); ?>js/jquery-1.11.1.min.js"></script>
		<script src="<?PHP echo base_url(); ?>js/min/plugins-min.js"></script>
		<script src="<?PHP echo base_url(); ?>js/min/app-min.js"></script>
		
	</body>

</html>
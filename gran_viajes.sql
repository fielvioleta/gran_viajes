-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2016 at 03:14 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gran_viajes`
--

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `edited_date` date NOT NULL,
  `deleted_date` date NOT NULL,
  `del_flag` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

CREATE TABLE `tours` (
  `id` int(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  `type` int(255) NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `edited_date` date DEFAULT NULL,
  `deleted_date` date DEFAULT NULL,
  `del_flag` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`id`, `place`, `type`, `img_path`, `created_date`, `edited_date`, `deleted_date`, `del_flag`) VALUES
(1, 'Bacolod', 0, '', '2016-03-13', NULL, NULL, 0),
(2, 'Baguio', 0, '', '2016-03-13', NULL, NULL, 0),
(3, 'Banaue', 0, '', '2016-03-13', NULL, NULL, 0),
(4, 'Batangas', 0, '', '2016-03-13', NULL, NULL, 0),
(5, 'Australia', 1, '', '2016-03-13', NULL, NULL, 0),
(6, 'Cambodia', 1, '', '2016-03-13', NULL, NULL, 0),
(7, 'Europe', 1, '', '2016-03-13', NULL, NULL, 0),
(8, 'Hong Kong', 1, '', '2016-03-13', NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tours`
--
ALTER TABLE `tours`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
